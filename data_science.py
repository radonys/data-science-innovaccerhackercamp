#!/usr/bin/python

from future.builtins import next

import os
import csv
import re
import logging
import optparse

import dedupe
from unidecode import unidecode

#Verbose Arguments
optp = optparse.OptionParser()
optp.add_option('-v', '--verbose', dest='verbose', action='count', help='Increase verbosity (specify multiple times for more)')
(opts, args) = optp.parse_args()

log_level = logging.WARNING 

if opts.verbose:
    if opts.verbose == 1:
        log_level = logging.INFO
    elif opts.verbose >= 2:
        log_level = logging.DEBUG

logging.getLogger().setLevel(log_level)

#Setup
input_file = 'DeduplicationProblem_Sample_Dataset.csv'
output_file = 'csv_innovacer_output.csv'
settings_file = 'csv_innovacer_learned_settings'
training_file = 'csv_innovacer_training.json'

def preProcess(column):

    try : # python 2/3 string differences
        column = column.decode('utf8')
    except AttributeError:
        pass

    column = unidecode(column)
    column = re.sub('  +', ' ', column)
    column = re.sub('\n', ' ', column)
    column = column.strip().strip('"').strip("'").lower().strip()

    if not column:
        column = None
    return column

def readData(filename):

    data_d = {}

    with open(filename) as f:
        reader = csv.DictReader(f)

        for row in reader:

            clean_row = [(k, preProcess(v)) for (k, v) in row.items()]
            row_id = int(row['Id'])
            data_d[row_id] = dict(clean_row)

    return data_d

print('Importing data ...')

#Assigning row ID's
with open(input_file, 'r') as input, open('temp.csv', 'w') as output:
    
    reader = csv.reader(input, delimiter = ',')
    writer = csv.writer(output, delimiter = ',')

    all = []
    row = next(reader)
    row.insert(0, 'Id')
    all.append(row)
    for k, row in enumerate(reader):
        all.append([str(k+1)] + row)
    writer.writerows(all)

data_d = readData("temp.csv")

if os.path.exists(settings_file):

    print('reading from', settings_file)

    with open(settings_file, 'rb') as f:
        deduper = dedupe.StaticDedupe(f)

else:
   	#Training

    fields = [
        {'field' : 'ln', 'type': 'String'},
        {'field' : 'dob', 'type': 'String'},
        {'field' : 'gn', 'type': 'Exact'},
        {'field' : 'fn', 'type': 'String'},
        ]

    deduper = dedupe.Dedupe(fields)
    deduper.sample(data_d)

    print('starting active labeling...')

    #Manual labeling 10 samples
    dedupe.consoleLabel(deduper)

    deduper.train()

    with open(training_file, 'w') as tf:
        deduper.writeTraining(tf)

    with open(settings_file, 'wb') as sf:
        deduper.writeSettings(sf)

threshold = deduper.threshold(data_d, recall_weight=1)

#Clustering

print('clustering...')
clustered_dupes = deduper.match(data_d, threshold)

print('# duplicate sets', len(clustered_dupes))

cluster_membership = {}
cluster_id = 0

for (cluster_id, cluster) in enumerate(clustered_dupes):
    id_set, scores = cluster
    cluster_d = [data_d[c] for c in id_set]
    canonical_rep = dedupe.canonicalize(cluster_d)
    for record_id, score in zip(id_set, scores):
        cluster_membership[record_id] = {
            "cluster id" : cluster_id,
            "canonical representation" : canonical_rep,
            "confidence": score
        }

singleton_id = cluster_id + 1

with open(output_file, 'w') as f_output, open("temp.csv") as f_input:

    writer = csv.writer(f_output)
    reader = csv.reader(f_input)

    heading_row = next(reader)
    heading_row.insert(0, 'Confidence_score')
    heading_row.insert(0, 'Cluster ID')
    canonical_keys = canonical_rep.keys()
    for key in canonical_keys:
        heading_row.append('canonical_' + key)

    writer.writerow(heading_row)

    for row in reader:

        row_id = int(row[0])

        if row_id in cluster_membership:

            cluster_id = cluster_membership[row_id]["cluster id"]
            canonical_rep = cluster_membership[row_id]["canonical representation"]
            row.insert(0, cluster_membership[row_id]['confidence'])
            row.insert(0, cluster_id)

            for key in canonical_keys:
                row.append(canonical_rep[key].encode('utf8'))
        else:
        	
            row.insert(0, None)
            row.insert(0, singleton_id)
            singleton_id += 1
            for key in canonical_keys:
                row.append(None)
        writer.writerow(row)
