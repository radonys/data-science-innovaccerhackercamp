# HackerCamp Summer 2018 Submission - Analytics

# Files in the repository:

1. data_science.py (Code)
2. DeduplicationProblem_Sample_Dataset.csv (Input Data)
3. csv_innovacer_output.csv (Output file)
4. csv_innovacer_training.json (Training Data)
5. temp.csv (Data file with row ID's, created during execution)
6. csv_innovacer_learned_settings (Weights and Predicates)
7. requirements.txt (Dependencies)
    
# Dependencies:
    
1. python3
2. unidecode
3. dedupe
    
# Running code:

1. Keep the input data file (DeduplicationProblem_Sample_Dataset.csv) and data_science.py in the same directory.
2. Execute the below command in command line:
        ```pip3 install -r requirements.txt```
3. Then, run:
        ```python3 data_science.py (optional verbose with -v (1/2) for more details)```
4. While execution, a command line console will come up to annotate some of the data (as input is not annotated) and will ask for resemblance of two entries. Annotation needs to be done, else we can't train the model.
5. Annotate ten samples for each 'yes' and 'no' resemblance and then finish it by pressing 'f' and enter.

# Output:

The output will contain the Cluster ID and Confidence score for each of the entries in the input file along with their canonical information.
    